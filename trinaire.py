#!/bin/env python3

import sys

def supprimeAccent(ligne):
  """ supprime les accents du texte source """
  accents = { 'a': ['á', 'à', 'â', 'ă', 'ã', 'ä'],
              'e': ['é', 'è', 'ê', 'ĕ', 'ẽ', 'ë'],
              'i': ['í', 'ì', 'î', 'ĭ', 'ĩ', 'ï'],
              'o': ['ó', 'ò', 'ô', 'ŏ', 'õ', 'ö'],
              'u': ['ú', 'ù', 'û', 'ŭ', 'ũ', 'ü'],
              'y': ['ý', 'ỳ', 'ŷ', 'ỹ', 'ÿ'] }
  for (char, accented_chars) in list(accents.items()):
    for accented_char in accented_chars:
      ligne = ligne.replace(accented_char, char)
  return ligne

def strto(arg):
  """Converti une chaîne de caractère de type texte en trinaire."""
  out=""
  arg = supprimeAccent(arg.lower())
  for c in arg:
    if(ord(c) in range(97,123)):
      out+="!" if (int((ord(c.lower())-96)/9) == 2) else ":" if (int((ord(c.lower())-96)/9) ==1) else "."
      out+="!" if (int(((ord(c.lower())-96)%9)/3) == 2) else ":" if (int(((ord(c.lower())-96)%9)/3) ==1) else "."
      out+="!" if (int((ord(c.lower())-96)%3) == 2) else ":" if (int((ord(c.lower())-96)%3) == 1) else "."
    else:
      out+="..."
  return out

def strfrom(arg):
  """Converti une chaîne de caractère de type trinaire en texte."""
  out = ""
  if(not len(arg)%3):
    for x in range(0,len(arg),3):
      y =  (arg[x]=="!" and 18 or arg[x]==":" and 9 or arg[x]=="." and 0) 
      y += (arg[x+1]=="!" and 6 or arg[x+1]==":" and 3 or arg[x+1]=="." and 0) 
      y += (arg[x+2]=="!" and 2 or arg[x+2]==":" and 1 or arg[x+2]=="." and 0)
      y = y and y + 96 or 32
      out+=chr(y)
  else:
    out="Le nombre de caractère n’est pas compatible, il doit être multiple de 3."
  return out

def usage():
  """Affiche comment utiliser le programe"""
  out="\n"
  out+="Le script ne fonctionne qu’avec python3 minimum. Il est incompatible avec python2.\n"
  out+="\n"
  out+="Usage :\n"
  out+="\n"
  out+="·Convertir un texte en trinaire :\n"
  out+="\tpython3 trinaire.py --to \"CHAÎNE DE CARACTÈREs\"\n"
  out+="\n"
  out+="·Convertir du trinaire en texte :\n"
  out+="\tpython3 trinaire.py --from \"CHAÎNE DE CARACTÈRES\"\n"
  return out

def start():
  """Démarre le processus et lit les arguments"""
  if(len(sys.argv) == 3):
    opt=sys.argv[1]
    arg=sys.argv[2]

    if opt == '--to':
      print(strto(arg))
    elif opt == "--from":
      print(strfrom(arg))
  else:
    print(usage())

if __name__ == "__main__":
  start()